import "reflect-metadata";
import express, {Express} from 'express';
import Config from "../server/xcomponents/config";
import Db from "../server/xcomponents/db";
import Services from "../server/xcomponents/services";
import Middlewares from "../server/xcomponents/middlewares";
import Routers from "../server/xcomponents/routers";

export class App  {

  public $router: Express;
  public $sv: Services;
  public $mw: Middlewares;
  public $db: Db;
  public $config: any;

  constructor() {
    this.$router = express();
  }

  public async boot(): Promise<void> {

    try {

      this.$config = await Config.make(this);
      this.$db = await Db.make(this);
      this.$mw = await Middlewares.make(this);
      this.$sv = await Services.make(this);
      await Routers.make(this);

    } catch (e) {
      console.log(e)
    }
  }

}

