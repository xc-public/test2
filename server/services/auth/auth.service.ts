import {BaseService} from "xc-core-ts";
import {App} from "../../app";
import {initStrategies} from "./strategies";
import {promisify} from "util";
import {isEmail} from 'validator';

import passport from 'passport';

import * as jwt from 'jsonwebtoken';

// todo : read from config
// const jwtOptions: any = {secretOrKey: 'dsdsdsds'}

// import {Strategy, ExtractJwt} from 'passport-jwt';
//
// const PassportLocalStrategy = require('passport-local').Strategy;

import bcrypt from 'bcryptjs';
import {Request} from "express";
import * as crypto from "crypto";

const {v4: uuidv4} = require('uuid');
export default class AuthService extends BaseService {
  // @ts-ignore
  private app: App;

  constructor(app: App) {
    super();
    this.app = app;
    initStrategies(app);
  }

  signin(req, res, next) {
    return new Promise((resolve, reject) => {
      passport.authenticate('local', {session: false}, async (err, user, info): Promise<any> => {

        try {

          if (!user || !user.email) {
            if (err) {
              return reject(err)
            }
            if (info) {
              return reject(info)
            }
            return reject({msg: 'Your signin has failed'});
          }

          await promisify((req as any).login.bind(req))(user);
          await this.generateRefreshToken(res, user.id);

          resolve({
            token: jwt.sign({
              email: user.email,
              firstname: user.firstname,
              lastname: user.lastname,
              id: user.id,
              roles: user.roles
            }, this.app.$config?.auth?.jwt?.secret, this.app.$config?.auth?.jwt?.options)
          });
        } catch (e) {
          console.log(e);
          reject(e);
        }

      })(req, res, next);
    });

  }

  async signup(userData: any) {

    let {email, password, firstname, lastname} = userData;

    if (!isEmail(email)) {
      throw new Error(`Invalid email address`);
    }
    let user = await this.app.$db.XcUsers.findOne({
      email: email
    });

    if (user) {
      throw new Error(`Email '${email}' already registered`);
    }

    // if (!validator.isEmail(email)) {
    //   throw new Error({msg: `Invalid email`})
    // }

    const salt = await promisify(bcrypt.genSalt)(10);
    password = await promisify(bcrypt.hash)(password, salt);
    const email_verification_token = uuidv4();


    let roles = 'user';
    const userex = await this.app.$db.XcUsers.findOne();
    if (!userex) {
      roles = 'admin,user'
    }


    await this.app.$db.XcUsers.insert({
      firstname, lastname,
      email,
      salt,
      password,
      roles,
      email_verification_token
    });

    user = await this.app.$db.XcUsers.findOne({
      email: email
    });
    try {
      // await this.transporter.sendMail({
      //   from: this.config?.mailer?.from,
      //   to: email,
      //   subject: "Verify email",
      //   html: require('./emailTemplate/verify')({
      //     verifyLink: `http://localhost:8080/email/verify/${user.email_verification_token}`
      //   })
      // })
    } catch (e) {
      console.log(e);
    }
    return user;

  }


  async passwordForgot(email: string) {

    if (!email) {
      throw new Error('Please enter your email address.');
    }

    let user = await this.app.$db.XcUsers.findOne({email});

    if (!user) {
      throw new Error('This email is not registered with us.');
    }

    const token = uuidv4();

    await this.app.$db.XcUsers.update({id: user.id}, {
      reset_password_token: token,
      reset_password_expires: new Date(Date.now() + (60 * 60 * 1000))
    });

    try {
      // await this.transporter.sendMail({
      //   from: this.config?.mailer?.from,
      //   to: user.email,
      //   subject: "Password Reset Link",
      //   text: `Visit following link to update your password : http://localhost:8080/password/reset/${token}.`,
      //   html: require('./emailTemplate/forgotPassword')({
      //     resetLink: `http://localhost:8080/password/reset/${token}`
      //   })
      // })

    } catch (e) {
      console.log(e);
    }
    console.log(`Password reset token : ${token}`)

  }


  async tokenValidate(token: string) {
    const user = await this.app.$db.XcUsers.findOne({reset_password_token: token});
    if (!user || !user.email) {
      throw new Error('Invalid reset url');
    }
    if (user.reset_password_expires < new Date()) {
      throw new Error('Password reset url expired');
    }

    return true
  }

  async passwordReset(token: string, data: { password: string }) {
    const user = await this.app.$db.XcUsers.findOne({reset_password_token: token});
    if (!user) {
      throw new Error('Invalid reset url');
    }
    if (user.reset_password_expires < new Date()) {
      throw new Error('Password reset url expired');
    }


    const salt = await promisify(bcrypt.genSalt)(10);
    const password = await promisify(bcrypt.hash)(data.password, salt);

    await this.app.$db.XcUsers.update({
      id: user.id
    }, {
      salt, password,
      reset_password_expires: null,
      reset_password_token: ''
    });

    return true;
  }


  async passwordChange(body: { currentPassword: string; newPassword: string; }, req): Promise<any> {

    const {currentPassword, newPassword} = body;
    if (req.isAuthenticated()) {
      if (!currentPassword || !newPassword) {
        throw new Error('Missing new/old password');
      }
      const user = await this.app.$db.XcUsers.findOne({email: req.user.email});
      const hashedPassword = await promisify(bcrypt.hash)(currentPassword, user.salt);
      if (hashedPassword !== user.password) {
        throw new Error('Current password is wrong')
      }

      const salt = await promisify(bcrypt.genSalt)(10);
      const password = await promisify(bcrypt.hash)(newPassword, salt);

      await this.app.$db.XcUsers.update({id: user.id}, {
        salt, password
      });
      return true;
    } else {
      throw new Error('Not authenticated');
    }
  }


  async emailVerification(token: string) {
    const user = await this.app.$db.XcUsers.findOne({email_verification_token: token});
    if (!user) {
      throw new Error('Invalid verification url');
    }

    await this.app.$db.XcUsers.update({id: user.id}, {
      email_verification_token: '',
      email_verified: true
    });

    return true;
  }

  async updateUser(id, body: {
    firstname: string;
    lastname: string;
  }) {
    await this.app.$db.XcUsers.update({
      id: id
    }, {
      firstname: body.firstname,
      lastname: body.lastname,
    })
    return true;
  }


  public catchErr(handler): any {
    return (req, res, next) => {
      Promise.resolve(handler.call(this, req, res, next)).catch(err => {
        res.status(500).json({msg: err.message})
      })
    }
  }


  public async generateRefreshToken(res, id): Promise<any> {
    const refreshToken = this.randomTokenString();

    await this.app.$db.XcUsers.update({
      id
    }, {
      refresh_token: refreshToken
    })

    this.setTokenCookie(res, refreshToken);
  }


  public async generateJwtTokenByRefreshToken(refreshToken, res): Promise<any> {
    console.log('token refresh')
    const user = await this.app.$db.XcUsers.findOne({
      refresh_token: refreshToken
    });

    if (!user) {
      throw new Error('Invalid refresh token');
    }

    await this.generateRefreshToken(res, user.id);

    return {
      token: jwt.sign({
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        id: user.id,
        roles: user.roles
      }, this.app.$config?.auth?.jwt?.secret, this.app.$config?.auth?.jwt?.options)
    };

  }


  private setTokenCookie(res, token) {
    // create http only cookie with refresh token that expires in 7 days
    const cookieOptions = {
      httpOnly: true,
      expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000)
    };
    res.cookie('refresh_token', token, cookieOptions);
  }

  private randomTokenString(): string {
    return crypto.randomBytes(40).toString('hex');
  }


}