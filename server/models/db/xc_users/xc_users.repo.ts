import {
  EntityRepository
} from "xc-typeorm";
import {
  XcUsersEntity
} from "./xc_users.entity";
import {
  XcGqlRepository
} from "xc-typeorm";


@EntityRepository(XcUsersEntity)
export default class XcUsers extends XcGqlRepository < XcUsersEntity > {}