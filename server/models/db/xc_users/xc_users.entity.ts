import {
  Column,
  Entity,
  PrimaryGeneratedColumn
} from "xc-typeorm";


@Entity("xc_users")
export class XcUsersEntity {

  @PrimaryGeneratedColumn({
    name: 'id',
  })
  id: number;

  @Column({
    name: 'email',
    nullable: true,
  })
  email: string | null;

  @Column({
    name: 'password',
    nullable: true,
  })
  password: string | null;

  @Column({
    name: 'salt',
    nullable: true,
  })
  salt: string | null;

  @Column({
    name: 'firstname',
    nullable: true,
  })
  firstname: string | null;

  @Column({
    name: 'lastname',
    nullable: true,
  })
  lastname: string | null;

  @Column({
    name: 'username',
    nullable: true,
  })
  username: string | null;

  @Column({
    name: 'reset_password_expires',
    nullable: true,
  })
  reset_password_expires: Date | null;

  @Column({
    name: 'reset_password_token',
    nullable: true,
  })
  reset_password_token: string | null;

  @Column({
    name: 'email_verification_token',
    nullable: true,
  })
  email_verification_token: string | null;

  @Column({
    name: 'email_verified',
    nullable: true,
  })
  email_verified: Date | null;

  @Column({
    name: 'roles',
    nullable: true,
  })
  roles: string | null;

  @Column({
    name: 'created_at',
    nullable: true,
  })
  created_at: Date | null;

  @Column({
    name: 'updated_at',
    nullable: true,
  })
  updated_at: Date | null

  @Column({
    name: 'refresh_token',
    nullable: true,
  })
  refresh_token: string | null;


}