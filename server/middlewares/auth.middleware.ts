import {Request, NextFunction, Response} from "express";
import { BaseMiddleware } from "xc-core-ts";

export default class AuthMiddleware extends BaseMiddleware {


  // @ts-ignore
  public async default(req: Request, res: Response, next: NextFunction): Promise<void> {
    console.log('global middleware')
    next();
  }

}
