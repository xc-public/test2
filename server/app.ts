import "reflect-metadata";
import express, {Express} from 'express';
import Config from "./xcomponents/config";
import Db from "./xcomponents/db";
import Services from "./xcomponents/services";
import Middlewares from "./xcomponents/middlewares";
import Routers from "./xcomponents/routers";
import cors from 'cors';

export class App {

  public $router: Express;
  public $sv: Services;
  public $mw: Middlewares;
  public $db: Db;
  public $config: any;

  constructor() {
    this.$router = express();
    this.$router.use(cors());
  }

  public async boot(): Promise<void> {

    try {

      this.$config = await Config.make(this);
      this.$db = await Db.make(this);
      this.$mw = await Middlewares.make(this);
      this.$sv = await Services.make(this);
      await Routers.make(this);

    } catch (e) {
      console.log(e)
    }
  }

}


new App().boot().then(() => {
  console.log('App started');
}).catch(e => {
  console.error('Some error occurred', e)
});
