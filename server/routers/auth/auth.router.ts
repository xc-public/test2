// import {Request, Response} from "express";
import {App} from "../../app";
import {BaseRouter, Route} from 'xc-core-ts';
import {promisify} from "util";
import passport from 'passport';

import * as jwt from 'jsonwebtoken';
import * as crypto from "crypto";
// import {Strategy, ExtractJwt} from 'passport-jwt';
//
// const PassportLocalStrategy = require('passport-local').Strategy;
//
// import bcrypt from 'bcryptjs';
//
// const {v4: uuidv4} = require('uuid');


export default class AuthRouter extends BaseRouter {

  private app: App;
  private jwtOptions: any = {secretOrKey: 'dsdsdsds'}

  constructor(app: App) {
    super();
    this.app = app;
    // this.jwtOptions.jwtFromRequest = ExtractJwt.fromHeader('xc-auth');
  }


  async signin(req, res, next) {
    res.json(await this.app.$sv.AuthService.signin(req, res, next) as any)
  }

  async signup(req, res, next) {
    let user;
    try {
      user = await this.app.$sv.AuthService.signup(req.body)
    } catch (e) {
      return next(e)
    }
    await promisify((req as any).login.bind(req))(user);

    user = (req as any).user;

    await this.app.$sv.AuthService.generateRefreshToken(res, user.id);

    res.json({
      token: jwt.sign({
        email: user.email,
        firstname: user.firstname,
        lastname: user.lastname,
        id: user.id,
        roles: user.roles
      }, this.app.$config?.auth?.jwt?.secret, this.app.$config?.auth?.jwt?.options)
    } as any);
  }

  async passwordForgot(req, res, next) {
    await this.app.$sv.AuthService.passwordForgot(req.body.email);
    res.json({msg: 'Check your email for password reset link.'});
  }

  async tokenValidate(req, res, next) {
    await this.app.$sv.AuthService.tokenValidate(req.params.tokenId);
    res.json(true);
  }


  async passwordReset(req, res, next) {
    await this.app.$sv.AuthService.passwordReset(req.params.tokenId, req.body);
    res.json({msg: 'Password reset successful'})
  }


  async passwordChange(req, res, next): Promise<any> {
    await this.app.$sv.AuthService.passwordChange(req.body, req);
    res.json({msg: 'Password updated successfully'})
  }


  async emailVerification(req, res, next) {
    await this.app.$sv.AuthService.emailVerification(req.params.token);
    res.json({msg: 'Email verified successfully'});
  }


  async me(req, res) {
    res.json(req?.session?.passport?.user ?? {});
  }

  async updateUser(req, res) {
    await this.app.$sv.AuthService.updateUser(req.user.id, req.body);
    res.json({msg: 'Updated successfully'});
  }


  async refreshToken(req, res) {
    try {
      res.json(await this.app.$sv.AuthService.generateJwtTokenByRefreshToken(req.cookies?.refresh_token, res));
    } catch (e) {
      res.status(400).json({msg: e.message})
    }
  }

  public $mapRoutes(): void {
    this.app.$router.use(passport.initialize())
    // this.app.$router.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    const jwtMiddleware = passport.authenticate('jwt', {session: false});

    const apiPrefix = 'v1';

    this.app.$router.get('/password/reset/:token', function (req, res) {
      res.render(__dirname + '/auth/resetPassword', {
        token: JSON.stringify(req.params?.token),
        baseUrl: `/api/${apiPrefix}/`
      });
    });
    this.app.$router.get('/email/verify/:token', function (req, res) {
      res.render(__dirname + '/auth/emailVerify', {
        token: JSON.stringify(req.params?.token),
        baseUrl: `/api/${apiPrefix}/`
      });
    });

    this.app.$router.get('/signin', function (_req, res) {
      res.render(__dirname + '/auth/signin', {
        baseUrl: `/api/${apiPrefix}/`
      });
    });

    this.app.$router.get('/signup', function (_req, res) {
      res.render(__dirname + '/auth/signup', {
        baseUrl: `/api/${apiPrefix}/`
      });
    });

    this.app.$router.post(`/api/${apiPrefix}/auth/signin`, this.catchErr(this.signin))
    this.app.$router.post(`/api/${apiPrefix}/auth/signup`, this.catchErr(this.signup))

    this.app.$router.post(`/api/${apiPrefix}/auth/password/forgot`, this.catchErr(this.passwordForgot))
    this.app.$router.post(`/api/${apiPrefix}/auth/token/validate/:tokenId`, this.catchErr(this.tokenValidate))
    this.app.$router.post(`/api/${apiPrefix}/auth/password/reset/:tokenId`, this.catchErr(this.passwordReset))
    this.app.$router.post(`/api/${apiPrefix}/user/password/change`, jwtMiddleware, this.catchErr(this.passwordChange))
    this.app.$router.post(`/api/${apiPrefix}/auth/email/validate/:tokenId`
      , this.catchErr(this.emailVerification));

    this.app.$router.put(`/api/${apiPrefix}/user`, jwtMiddleware, this.catchErr(this.updateUser));


    // middleware for setting passport user( for treating non-authenticated user as guest)
    this.app.$router.use(async (req, res, next) => {
      const user = await new Promise(resolve => {
        passport.authenticate('jwt', {session: false}, (_err, user, _info) => {
          if (user) {
            return resolve(user);
          }
          resolve({roles: 'guest'})
        })(req, res, next);
      })

      await promisify((req as any).login.bind(req))(user);
      next();
    });

    this.app.$router.get(`/api/${apiPrefix}/user/me`, this.catchErr(this.me));

    this.app.$router.post(`/api/${apiPrefix}/auth/refresh-token`, this.refreshToken)

  }


}
