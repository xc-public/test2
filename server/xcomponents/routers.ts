import glob from "glob";
import * as path from "path";
import {App} from "../app";
import bodyParser from 'body-parser';

import cookieParser from 'cookie-parser';
import XcToolApi from "xc-lib-gui";

export default class Routers {

  public static async make(app: App): Promise<any> {

    try {

      app.$router.use(bodyParser.urlencoded({extended: true}));
      app.$router.use(bodyParser.json());
      app.$router.use(cookieParser());

      const authRoutePath = path.join(__dirname, '../routers/auth/*.router.{ts,js}');
      const ignoreRoutes = [authRoutePath];

      for (const routerPath of glob.sync(authRoutePath)) {
        const router = (await import(routerPath)).default;
        const routerInstance: any = new router(app);
        routerInstance.$mapRoutes();
      }


      for (const routerPath of glob.sync(path.join(__dirname, '../routers/**/*.router.{ts,js}'), {ignore: ignoreRoutes})) {
        const router = (await import(routerPath)).default;
        const routerInstance: any = new router(app);
        routerInstance.$mapRoutes();
      }

      app.$router.use(await new XcToolApi(app.$config.xcConfig).expressMiddleware());

      app.$router.listen(8080, () => {
        console.log('Running on port 8080')
      })


      return app.$router;

    } catch (e) {
      console.log(e)
    }
  }

}
