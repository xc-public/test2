import AuthMiddleware from "../middlewares/auth.middleware";
import {App} from "../app";
import glob from "glob";
import * as path from "path";
import {Handler} from "express";


export default class Middlewares {

  public static async make(app: App): Promise<Middlewares> {

    if (Middlewares.instanceRef) {
      return Middlewares.instanceRef
    }

    const middlewares = Middlewares.instanceRef = new Middlewares();

    try {
      middlewares.$global = {};
      for (const middlewarePath of glob.sync(path.join(__dirname, '../middlewares/**/*.middleware.{ts,js}'))) {
        const middleware = (await import(middlewarePath)).default;
        middlewares.$global[middleware.name] = new middleware(app);
      }

      for (const dbConfig of app.$config.dbs) {
        for (const middlewarePath of glob.sync(path.join(__dirname, `../routers/${dbConfig.meta.dbAlias}/**/*.middleware.{ts,js}`))) {
          const middleware = (await import(middlewarePath)).default;
          middlewares[dbConfig.meta.dbAlias] = middlewares[dbConfig.meta.dbAlias] || {};
          middlewares[dbConfig.meta.dbAlias][middleware.name] = new middleware(app);
        }
      }
    } catch (e) {
      console.log(e)
    }
    return Object.freeze(middlewares);
  }

  public getGlobalMiddlewares(): Handler[] {
    return Object.values(this.$global).map(mw => mw.default);
  }

  private static instanceRef: Middlewares;

  public $global: { AuthMiddleware?: AuthMiddleware };

  private constructor() {
  }

}
