import {App} from "../app";
import {createConnection, XcDbUtils} from "xc-typeorm";
import glob from "glob";
import * as path from "path";
import XcUsers from "../models/db/xc_users/xc_users.repo";
import {Migrator} from 'xc-migrator';

export default class Db {

  public static async make(app: App): Promise<Db> {

    const models = new Db();

    try {


      const config = app.$config.dbs[0];
      models[config.meta.dbAlias] = {};
      const c = XcDbUtils.makeTypeormConfigFromKnexConfig(config);
      const connection = await createConnection(c);

      await this.syncMigration(app, config);

      try {

        for (const modelsPath of glob.sync(path.join(__dirname, `../models/${config.meta.dbAlias}/**/*.repo.{ts,js}`))) {
          const model = (await import(modelsPath)).default;
          models[model.name] = connection.getCustomRepository(model);

        }

      } catch (e) {
        console.log(e)
      }


    } catch (e) {
      console.log(e)
    }
    return Object.freeze(models);
  }

  public XcUsers: XcUsers;


  private static async syncMigration(app, config) {
    try {

      /* Update database migrations */
      const migrator = new Migrator();

      await migrator.sync({
        folder: process.cwd(),
        env: app.$config.env,
        dbAlias: config.meta.dbAlias
      });

      await migrator.migrationsUp({
        folder: process.cwd(),
        env: app.$config.env,
        dbAlias: config.meta.dbAlias,
        migrationSteps: 99999,
        sqlContentMigrate: 1,
      });

      console.log(`Migrations completed for \n\r\n\r\tDB Alias\t:\t${config.meta?.dbAlias}\n\r\tDatabase\t:\t${config?.connection?.database}\n\r`)


    } catch (e) {
      console.log(`Migrations failed for \n\r\n\r\tDB Alias\t:\t${config.meta?.dbAlias}\n\r\tDatabase\t:\t${config?.connection?.database}\n\r`)
    }
  }

}
