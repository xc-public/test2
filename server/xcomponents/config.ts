import {App} from "../app";

import fs from 'fs';
import {promisify} from 'util';
import path from 'path';
import Handlebars from 'handlebars';


export default class Config {


  // @ts-ignore
  public static async make(app: App): Promise<any> {

    const config: any = {};

    try {

      /* get database config */
      const dbConfig: any = await promisify(fs.readFile)(
        path.join(__dirname, '..', '..', 'config.xc.json'),
        "utf8"
      );

      /* replace environment variables with its values */
      // tslint:disable-next-line:variable-name
      config.xcConfig = JSON.parse(dbConfig, (_key, value) => {
        return typeof value === 'string' ? Handlebars.compile(value, {noEscape: true})(process.env) : value;
      });


      /* find and set current environment value - defaults to dev */
      const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'dev';
      config.env = env;

      /* load default config */
      Object.assign(config, (await import('../config/default')).default);

      /* load environment specific config */
      const envConfig = (await import(`../config/${env}`)).default;
      Object.assign(config, envConfig);

      /* load db connections params */
      config.dbs = config.xcConfig.envs[env].db;

      // todo : move to xc.config
      config.xcConfig.auth = config.auth;
    } catch (e) {
      console.log(e);
      throw e;
    }
    return Object.freeze(config);

  }

}
