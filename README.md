# todo
- findOne -> composite primary key
- read, udpate, delete -> composite primary key
- bulk update and delete -> composite primary key

- app to be passed in constructor

- adding components
    - adding module
    - adding module without table
        - adding individual files
            - adding router
            - adding middleware
            - adding service
            - adding model


- adding component
    - to bind to app context
        - each component has a loader
        - each component has a list if there are multiple objects to be loaded
    - 






# CLI

xc new projectName





# XcRepository 

- Count by xwhere
- Bulk update - *not implemented*
- Bulk delete - *with composite pk to be done*
- Pagination params validation
- Error message based on environment
- Todo : Limiting nested relation (hasMany)
- Todo : *routes* array => class




# Xc TS datamapper - library

- Library referring `typeorm` withing the library project(node_modules) and it causing different class instance.  
- Inside main project `typeorm` will compare it with class instance with `typeorm` within project(node_modules).   
- To avoid the issue we exported `typeorm` modules from library. 
